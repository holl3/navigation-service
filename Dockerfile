# => Build container
FROM openjdk:11.0-jdk-slim AS BUILD_CONTAINER

WORKDIR /build
COPY ./ ./

RUN ./gradlew build

# => Run container
FROM openjdk:11-jre-slim

COPY --from=BUILD_CONTAINER /build/build/libs/navigation-service-1.0-SNAPSHOT.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]
