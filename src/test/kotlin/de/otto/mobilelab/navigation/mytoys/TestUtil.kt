package de.otto.mobilelab.navigation.mytoys

fun testMyToysNavigation() =
    MyToysNavigation(
        navigationEntries = listOf(
            MyToysNavigationEntry(
                type = "selection",
                label = "Sortiment",
                children = listOf(
                    MyToysNavigationEntry(
                        type = "node",
                        label = "Alter",
                        children = listOf(
                            MyToysNavigationEntry(
                                type = "node",
                                label = "Baby & Kleinkind",
                                children = listOf(
                                    MyToysNavigationEntry(
                                        type = "link",
                                        label = "0-6 Monate",
                                        url = "http://www.mytoys.de/0-6-months/"
                                    ),
                                    MyToysNavigationEntry(
                                        type = "link",
                                        label = "7-12 Monate",
                                        url = "http://www.mytoys.de/7-12-months/"
                                    ),
                                    MyToysNavigationEntry(
                                        type = "link",
                                        label = "13-24 Monate",
                                        url = "http://www.mytoys.de/13-24-months/"
                                    ),
                                )
                            ), MyToysNavigationEntry(
                                type = "node", label = "Kindergarten", children = listOf(
                                    MyToysNavigationEntry(
                                        type = "link",
                                        label = "2-3 Jahre",
                                        url = "http://www.mytoys.de/24-47-months/"
                                    ),
                                    MyToysNavigationEntry(
                                        type = "link",
                                        label = "4-5 Jahre",
                                        url = "http://www.mytoys.de/48-71-months/"
                                    ),
                                )
                            )
                        )
                    )
                )
            )
        )
    )

fun testMyToysNavigationSmall() =
    MyToysNavigation(
        navigationEntries = listOf(
            MyToysNavigationEntry(
                type = "selection",
                label = "Sortiment",
                children = listOf(
                    MyToysNavigationEntry(
                        type = "link",
                        label = "2-3 Jahre",
                        url = "http://www.mytoys.de/24-47-months/"
                    )
                )
            )
        )
    )


