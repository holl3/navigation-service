package de.otto.mobilelab.navigation.mytoys

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class NavigationServiceTest {

    private val navigationService = NavigationService()


    @Test
    fun `find parent in a one dimensional tree`() {
        // given
        val navigation = MyToysNavigation(
            navigationEntries = listOf(
                MyToysNavigationEntry(
                    type = "selection",
                    label = "Sortiment",
                    children = listOf(
                        MyToysNavigationEntry(
                            type = "link",
                            label = "2-3 Jahre",
                            url = "http://www.mytoys.de/24-47-months/"
                        )
                    )
                )
            )
        )

        // when
        val result = navigationService.findNavigationBelowRoot(navigation, "Sortiment")

        // then
        assertThat(result).containsExactlyInAnyOrder(
            MyToysNavigationEntry(
                type = "link",
                label = "2-3 Jahre",
                url = "http://www.mytoys.de/24-47-months/"
            )
        )
    }

    @Test
    fun `find node without children`() {
        // given
        val navigation = testMyToysNavigationSmall()

        // when
        val result = navigationService.findNavigationBelowRoot(navigation, "2-3 Jahre")

        // then
        assertThat(result).containsExactlyInAnyOrder(
            MyToysNavigationEntry(type = "link", label = "2-3 Jahre", url = "http://www.mytoys.de/24-47-months/")
        )
    }

    @Test
    fun `find parent in bigger tree `() {
        // given
        val navigation = testMyToysNavigation()

        // when
        val result = navigationService.findNavigationBelowRoot(navigation, "Kindergarten")

        // then
        assertThat(result).containsExactlyInAnyOrder(
            MyToysNavigationEntry(type = "link", label = "2-3 Jahre", url = "http://www.mytoys.de/24-47-months/"),
            MyToysNavigationEntry(type = "link", label = "4-5 Jahre", url = "http://www.mytoys.de/48-71-months/")
        )
    }

    @Test
    fun `do not find node with root label`() {
        // given
        val navigation = testMyToysNavigationSmall()

        // when
        val result = navigationService.findNavigationBelowRoot(navigation, "Grundschulalter")

        // then
        assertThat(result).isEmpty()
    }

}