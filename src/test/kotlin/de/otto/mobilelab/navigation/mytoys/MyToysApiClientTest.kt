package de.otto.mobilelab.navigation.mytoys

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.containing
import de.otto.mobilelab.navigation.SpringContextTestBase
import org.apache.http.HttpHeaders
import org.apache.http.entity.ContentType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ClassPathResource
import org.springframework.util.StreamUtils
import java.lang.IllegalStateException
import java.nio.charset.StandardCharsets

class MyToysApiClientTest : SpringContextTestBase() {


    @Autowired
    private lateinit var myToysApiClient: MyToysApiClient

    @BeforeEach
    fun beforeEach() {
        WireMock.reset()
    }

    @Test
    fun `should receive valid response`() {
        // given
        WireMock.stubFor(
            WireMock.get("/v1/mytoys/navigation").withHeader("x-api-key", containing("api-key")).willReturn(
                WireMock.aResponse()
                    .withStatus(200)
                    .withBody(
                        loadResource("/mytoys/successful_response.json")
                    )
                    .withHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.mimeType)
            )
        )

        // when
        val result = myToysApiClient.getNavigationStructure()

        // then
        assertThat(result).isEqualTo(
            MyToysNavigation(
                navigationEntries = listOf(
                    MyToysNavigationEntry(
                        type = "section",
                        label = "Sortiment",
                        children = listOf(
                            MyToysNavigationEntry(
                                type = "node",
                                label = "Alter",
                                children = listOf(
                                    MyToysNavigationEntry(
                                        type = "node",
                                        label = "Baby & Kleinkind",
                                        children = listOf(
                                            MyToysNavigationEntry(
                                                type = "link",
                                                label = "0-6 Monate",
                                                url = "http://www.mytoys.de/0-6-months/"
                                            ),
                                            MyToysNavigationEntry(
                                                type = "link",
                                                label = "7-12 Monate",
                                                url = "http://www.mytoys.de/7-12-months/"
                                            ),
                                            MyToysNavigationEntry(
                                                type = "link",
                                                label = "13-24 Monate",
                                                url = "http://www.mytoys.de/13-24-months/"
                                            ),
                                        )
                                    ), MyToysNavigationEntry(
                                        type = "node", label = "Kindergarten", children = listOf(
                                            MyToysNavigationEntry(
                                                type = "link",
                                                label = "2-3 Jahre",
                                                url = "http://www.mytoys.de/24-47-months/"
                                            ),
                                            MyToysNavigationEntry(
                                                type = "link",
                                                label = "4-5 Jahre",
                                                url = "http://www.mytoys.de/48-71-months/"
                                            ),
                                        )
                                    )
                                )
                            )
                        )

                    )
                )
            )
        )
    }

    @Test
    fun `should catch errors`(){
        WireMock.stubFor(
            WireMock.get("/v1/mytoys/navigation").withHeader("x-api-key", containing("api-key")).willReturn(
                WireMock.aResponse()
                    .withStatus(403)
                    .withStatusMessage("don't find the endpoint")
            )
        )

        // when
        val e = assertThrows<IllegalStateException> {  myToysApiClient.getNavigationStructure()}

        // then
        assertThat(e.message). isEqualTo("could not get navigation from backend. got message with status: [403], and message: [don't find the endpoint]")
    }
}

fun loadResource(resourceName: String): String {
    return StreamUtils.copyToString(ClassPathResource(resourceName).inputStream, StandardCharsets.UTF_8)
}