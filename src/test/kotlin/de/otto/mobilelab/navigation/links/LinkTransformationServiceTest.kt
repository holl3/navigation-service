package de.otto.mobilelab.navigation.links

import de.otto.mobilelab.navigation.mytoys.MyToysNavigationEntry
import de.otto.mobilelab.navigation.mytoys.testMyToysNavigation
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class LinkTransformationServiceTest {

    private val linkTransformationService = LinkTransformationService()


    @Test
    fun `transform navigation structure into link list`() {
        //given
        val navigation = testMyToysNavigation()

        // when
        val result = linkTransformationService.createLinkListFromNavigationStructure(navigation)

        // then
        Assertions.assertThat(result).containsExactlyInAnyOrder(
            Link(label = "Sortiment - Alter - Baby & Kleinkind - 0-6 Monate", url = "http://www.mytoys.de/0-6-months/"),
            Link(label = "Sortiment - Alter - Baby & Kleinkind - 7-12 Monate", url = "http://www.mytoys.de/7-12-months/"),
            Link(label = "Sortiment - Alter - Baby & Kleinkind - 13-24 Monate", url = "http://www.mytoys.de/13-24-months/"),
            Link(label = "Sortiment - Alter - Kindergarten - 2-3 Jahre", url = "http://www.mytoys.de/24-47-months/"),
            Link(label = "Sortiment - Alter - Kindergarten - 4-5 Jahre", url = "http://www.mytoys.de/48-71-months/")
        )
    }


    @Test
    fun `transform one node into link list`() {
        //given
        val navigation = MyToysNavigationEntry(
            type = "link",
            label = "0-6 Monate",
            url = "http://www.mytoys.de/0-6-months/"
        )

        // when
        val result = linkTransformationService.createLinkListFromMyToysNavigationEntries(navigation, null)

        // then
        Assertions.assertThat(result).containsExactlyInAnyOrder(
            Link(label = "0-6 Monate", url = "http://www.mytoys.de/0-6-months/"),
        )
    }


}