package de.otto.mobilelab.navigation.links

import com.nhaarman.mockitokotlin2.*
import de.otto.mobilelab.navigation.mytoys.MyToysApiClient
import de.otto.mobilelab.navigation.mytoys.MyToysNavigationEntry
import de.otto.mobilelab.navigation.mytoys.NavigationService
import de.otto.mobilelab.navigation.mytoys.testMyToysNavigation
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class LinkServiceTest {

    private val myToysApiClient: MyToysApiClient = mock()
    private val navigationService: NavigationService = mock()
    private val linkTransformationService: LinkTransformationService = mock()
    private val sortService: SortService = mock()

    private val linkService = LinkService(
        myToysApiClient = myToysApiClient,
        navigationService = navigationService,
        linkTransformationService = linkTransformationService,
        sortService = sortService
    )

    @Test
    fun `create link list from navigation structure`() {
        // given
        val navigation = testMyToysNavigation()
        whenever(myToysApiClient.getNavigationStructure()).thenReturn(navigation)
        val linkList = listOf(
            Link(label = "Sortiment - Alter - Baby & Kleinkind - 0-6 Monate", url = "http://www.mytoys.de/0-6-months/"),
            Link(label = "Sortiment - Alter - Baby & Kleinkind - 7-12 Monate", url = "http://www.mytoys.de/7-12-months/"),
        )
        whenever(linkTransformationService.createLinkListFromNavigationStructure(any())).thenReturn(linkList)
        whenever(sortService.sortLinkList(any(), any())).thenReturn(linkList)

        // when
        val result = linkService.getLinkList(sortOrder = emptyMap())

        // then
        assertThat(result).containsExactlyInAnyOrder(
            Link(label = "Sortiment - Alter - Baby & Kleinkind - 0-6 Monate", url = "http://www.mytoys.de/0-6-months/"),
            Link(label = "Sortiment - Alter - Baby & Kleinkind - 7-12 Monate", url = "http://www.mytoys.de/7-12-months/"),
        )
        verify(linkTransformationService).createLinkListFromNavigationStructure(navigation)
    }

    @Test
    fun `create link list with given paren name`() {
        // given
        val navigation = testMyToysNavigation()
        whenever(myToysApiClient.getNavigationStructure()).thenReturn(navigation)
        val navigationNode1 = MyToysNavigationEntry(
            type = "link",
            label = "2-3 Jahre",
            url = "http://www.mytoys.de/24-47-months/"
        )
        val navigationNode2 = MyToysNavigationEntry(
            type = "link",
            label = "4-5 Jahre",
            url = "http://www.mytoys.de/48-71-months/"
        )
        whenever(navigationService.findNavigationBelowRoot(tree = any(), rootLabel = any())).thenReturn(
            listOf(navigationNode1, navigationNode2)
        )
        val link1 = Link(label = "2-3 Jahre", url = "http://www.mytoys.de/24-47-months/")
        val link2 = Link(label = "4-5 Jahre", url = "http://www.mytoys.de/48-71-months/")
        whenever(linkTransformationService.createLinkListFromMyToysNavigationEntries(node = any(), parentLabel = anyOrNull()))
            .thenReturn(listOf(link1))
            .thenReturn(listOf(link2))
        whenever(sortService.sortLinkList(linkList = any(), sortOrder = any())).thenReturn(listOf(link1, link2))

        // when
        val result = linkService.getLinkList("Kindergarten", emptyMap())

        // then
        verify(navigationService).findNavigationBelowRoot(navigation, "Kindergarten")
        assertThat(result).containsExactlyInAnyOrder(
            Link(label = "2-3 Jahre", url = "http://www.mytoys.de/24-47-months/"),
            Link(label = "4-5 Jahre", url = "http://www.mytoys.de/48-71-months/")

        )
        verify(linkTransformationService).createLinkListFromMyToysNavigationEntries(navigationNode1, null)
        verify(linkTransformationService).createLinkListFromMyToysNavigationEntries(navigationNode2, null)
    }
}
