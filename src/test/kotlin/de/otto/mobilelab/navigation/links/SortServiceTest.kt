package de.otto.mobilelab.navigation.links

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class SortServiceTest {


    private val sortService = SortService()

    @Test
    fun `do not sort when no order is given`() {
        // given
        val link1 = Link(label = "4-5 Jahre", url = "http://acv.de")
        val link2 = Link(label = "2-3 Jahre", url = "http://bcd.de")
        val linkList = listOf(link1, link2)

        // when
        val result = sortService.sortLinkList(linkList, emptyMap())

        // then
        assertThat(result).containsExactly(link1, link2)
    }

    @Test
    fun `do not sort when order contains no valid key`() {
        // given
        val link1 = Link(label = "4-5 Jahre", url = "http://acv.de")
        val link2 = Link(label = "2-3 Jahre", url = "http://bcd.de")
        val linkList = listOf(link1, link2)

        // when
        val result = sortService.sortLinkList(linkList, mapOf("notValid" to "asc"))

        // then
        assertThat(result).containsExactly(link1, link2)
    }

    @Test
    fun `sort with one order key and asc`(){
        // given
        val link1 = Link(label = "4-5 Jahre", url = "http://acv.de")
        val link2 = Link(label = "2-3 Jahre", url = "http://bcd.de")
        val linkList = listOf(link1, link2)

        // when
        val result = sortService.sortLinkList(linkList, mapOf("label" to "asc"))

        // then
        assertThat(result).containsExactly(link2, link1)
    }

    @Test
    fun `sort with one order key and desc`(){
        // given
        val link1 = Link(label = "4-5 Jahre", url = "http://acv.de")
        val link2 = Link(label = "2-3 Jahre", url = "http://bcd.de")
        val linkList = listOf(link1, link2)

        // when
        val result = sortService.sortLinkList(linkList, mapOf("url" to "desc"))

        // then
        assertThat(result).containsExactly(link2, link1)
    }

    @Test
    fun `sort with more then one order keys`(){
        // given
        val link1 = Link(label = "4-5 Jahre", url = "http://acv.de")
        val link2 = Link(label = "2-3 Jahre", url = "http://bcd.de")
        val link3 = Link(label = "1-2 Jahre", url = "http://abc.de")
        val linkList = listOf(link1, link2, link3)

        // when
        val result = sortService.sortLinkList(linkList, mapOf("url" to "asc", "label" to "asc"))

        // then
        assertThat(result).containsExactly(link3, link1, link2)
    }

}