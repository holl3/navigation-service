package de.otto.mobilelab.navigation.links

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.setup.MockMvcBuilders

class LinkControllerTest {

    private val linkService: LinkService = mock()
    private val linkController = LinkController(linkService)
    private val mockMvc = MockMvcBuilders.standaloneSetup(linkController).build()

    @Test
    fun `return list of links`() {
        //given
        whenever(linkService.getLinkList(parentName = anyOrNull(), sortOrder = any())).thenReturn(
            listOf(
                Link(label = "0-6 Monate", url = "http://www.mytoys.de/0-6-months/"),
                Link(label = "7-12 Monate", url = "http://www.mytoys.de/7-12-months/")
            )
        )

        // when
        mockMvc.perform(MockMvcRequestBuilders.get("/links"))
            //then
            .andExpect(MockMvcResultMatchers.status().is2xxSuccessful)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                content().json(
                    """[
                                      {
                                        "label": "0-6 Monate",
                                        "url": "http://www.mytoys.de/0-6-months/"
                                      },
                                      {
                                        "label": "7-12 Monate",
                                        "url": "http://www.mytoys.de/7-12-months/"
                                      }
                                    ]""", true
                )
            )
    }

    @Test
    fun `return empty list `() {
        // given
        whenever(linkService.getLinkList(parentName = anyOrNull(), sortOrder = any())).thenReturn(listOf())

        // when
        mockMvc.perform(MockMvcRequestBuilders.get("/links"))
            //then
            .andExpect(MockMvcResultMatchers.status().is2xxSuccessful)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json("[]", true))
    }

    @Test
    fun `consume parent parameter`() {
        // given
        whenever(linkService.getLinkList(any(), any())).thenReturn(
            listOf(
                Link(label = "0-6 Monate", url = "http://www.mytoys.de/0-6-months/"),
                Link(label = "7-12 Monate", url = "http://www.mytoys.de/7-12-months/")
            )
        )

        // when
        mockMvc.perform(
            MockMvcRequestBuilders.get("/links")
                .param("parent", "Kindergarten")
        )
            //then
            .andExpect(MockMvcResultMatchers.status().is2xxSuccessful)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                content().json(
                    """[
                                      {
                                        "label": "0-6 Monate",
                                        "url": "http://www.mytoys.de/0-6-months/"
                                      },
                                      {
                                        "label": "7-12 Monate",
                                        "url": "http://www.mytoys.de/7-12-months/"
                                      }
                                    ]""", true
                )
            )

        verify(linkService).getLinkList("Kindergarten", emptyMap())
    }

    @Test
    fun `decompose one oder element`() {
        // given
        whenever(linkService.getLinkList(parentName = anyOrNull(), sortOrder = any())).thenReturn(listOf())

        // when
        mockMvc.perform(
            MockMvcRequestBuilders.get("/links")
                .param("sort", "url:asc")
        )
            //then
            .andExpect(MockMvcResultMatchers.status().is2xxSuccessful)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json("[]", true))

        verify(linkService).getLinkList(parentName = null, sortOrder = mapOf("url" to "asc"))
    }

    @Test
    fun `decompose multiple oder elements`() {
        // given
        whenever(linkService.getLinkList(parentName = anyOrNull(), sortOrder = any())).thenReturn(listOf())

        // when
        mockMvc.perform(
            MockMvcRequestBuilders.get("/links")
                .param("sort", "url:asc,link:desc")
        )
            //then
            .andExpect(MockMvcResultMatchers.status().is2xxSuccessful)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json("[]", true))

        verify(linkService).getLinkList(parentName = null, sortOrder = mapOf("url" to "asc", "link" to "desc"))
    }

    @Test
    fun `decompose one oder element with default direction`() {
        // given
        whenever(linkService.getLinkList(parentName = anyOrNull(), sortOrder = any())).thenReturn(listOf())

        // when
        mockMvc.perform(
            MockMvcRequestBuilders.get("/links")
                .param("sort", "url")
        )
            //then
            .andExpect(MockMvcResultMatchers.status().is2xxSuccessful)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json("[]", true))

        verify(linkService).getLinkList(parentName = null, sortOrder = mapOf("url" to "asc"))
    }

    @Test
    fun `get empty map when sortParameters are empty`() {
        // given
        whenever(linkService.getLinkList(parentName = anyOrNull(), sortOrder = any())).thenReturn(listOf())

        // when
        mockMvc.perform(
            MockMvcRequestBuilders.get("/links")
                .param("sort", "")
        )
            //then
            .andExpect(MockMvcResultMatchers.status().is2xxSuccessful)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json("[]", true))

        verify(linkService).getLinkList(parentName = null, sortOrder = emptyMap())
    }

}