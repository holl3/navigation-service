package de.otto.mobilelab.navigation.health

import org.junit.jupiter.api.Test
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders

class HealthCheckTest {

    private val healthCheck = HealthCheck()
    private val mockMvc = MockMvcBuilders.standaloneSetup(healthCheck).build()

    @Test
    fun `should return healthy`() {

        // when
        mockMvc.perform(get("/health"))
            //then
            .andExpect(status().is2xxSuccessful)
            .andExpect(content().string("healthy"))
    }
}