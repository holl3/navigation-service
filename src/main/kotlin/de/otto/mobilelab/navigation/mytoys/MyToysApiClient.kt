package de.otto.mobilelab.navigation.mytoys

import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.OkHttpClient
import okhttp3.Request
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class MyToysApiClient(
    private val restClient: OkHttpClient,
    private val objectMapper: ObjectMapper,
    @Value("\${myToys.url}")
    private val url: String,
    @Value("\${myToys.api-key}")
    private val apiKey: String,
    @Value("\${myToys.path}")
    private val path: String
) {

    fun getNavigationStructure(): MyToysNavigation {
        val request = Request.Builder()
            .addHeader("x-api-key", apiKey)
            .url("$url$path")
            .build()

        val response = restClient.newCall(request).execute()

        if (response.isSuccessful) {
            return objectMapper.readValue(response.body?.string(), MyToysNavigation::class.java)
        } else {
            throw IllegalStateException("could not get navigation from backend. got message with status: [${response.code}], and message: [${response.message}]")
        }
    }

}

