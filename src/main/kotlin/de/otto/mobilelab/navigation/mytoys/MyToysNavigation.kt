package de.otto.mobilelab.navigation.mytoys

data class MyToysNavigation(
    val navigationEntries: List<MyToysNavigationEntry>
)

data class MyToysNavigationEntry(
    val type: String,
    val label: String,
    val url: String? = null,
    val children: List<MyToysNavigationEntry>? = null
)