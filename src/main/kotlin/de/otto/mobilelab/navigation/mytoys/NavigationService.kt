package de.otto.mobilelab.navigation.mytoys

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class NavigationService {

    fun findNavigationBelowRoot(tree: MyToysNavigation, rootLabel: String): List<MyToysNavigationEntry> {
        val list = mutableListOf<MyToysNavigationEntry>()

        log.info("try to find root element with label $rootLabel")

        tree.navigationEntries.forEach { node ->
            list.addAll(findNodesBelowRoot(node, rootLabel))
        }

        return list
    }

    private fun findNodesBelowRoot(node: MyToysNavigationEntry, rootLabel: String): List<MyToysNavigationEntry> {
        val list = mutableListOf<MyToysNavigationEntry>()

        if (node.label == rootLabel && !node.children.isNullOrEmpty()) {
            return node.children
        } else if (node.label == rootLabel && node.children.isNullOrEmpty()) {
            return listOf(node)
        } else {
            node.children?.forEach {
                list.addAll(findNodesBelowRoot(it, rootLabel))
            }
        }

        return list
    }


    companion object {
        private val log = LoggerFactory.getLogger(NavigationService::class.java)
    }

}