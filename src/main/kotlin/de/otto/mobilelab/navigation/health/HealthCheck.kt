package de.otto.mobilelab.navigation.health

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HealthCheck {

    @GetMapping(value = ["/health"])
    fun heath(): ResponseEntity<String> {
        return ResponseEntity.ok("healthy")
    }
}