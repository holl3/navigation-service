package de.otto.mobilelab.navigation.links

import de.otto.mobilelab.navigation.mytoys.MyToysApiClient
import de.otto.mobilelab.navigation.mytoys.MyToysNavigation
import de.otto.mobilelab.navigation.mytoys.NavigationService
import org.springframework.stereotype.Service

@Service
class LinkService(
    private val myToysApiClient: MyToysApiClient,
    private val navigationService: NavigationService,
    private val linkTransformationService: LinkTransformationService,
    private val sortService: SortService
) {

    fun getLinkList(parentName: String? = null, sortOrder: Map<String, String>): List<Link> {

        val navigationStructure = myToysApiClient.getNavigationStructure()
        val linkList = generateLinkList(parentName, navigationStructure)

        return sortService.sortLinkList(linkList, sortOrder)
    }

    private fun generateLinkList(parentName: String?, navigationStructure: MyToysNavigation): List<Link> {

        return if (parentName != null)
            getLinksStartsByParent(navigationStructure, parentName)
        else
            linkTransformationService.createLinkListFromNavigationStructure(navigationStructure)
    }

    private fun getLinksStartsByParent(navigationStructure: MyToysNavigation, parenName: String): List<Link> {
        val subNavigations = navigationService.findNavigationBelowRoot(navigationStructure, parenName)

        return subNavigations.flatMap { linkTransformationService.createLinkListFromMyToysNavigationEntries(it, null) }
    }

}