package de.otto.mobilelab.navigation.links

import de.otto.mobilelab.navigation.mytoys.MyToysNavigation
import de.otto.mobilelab.navigation.mytoys.MyToysNavigationEntry
import org.springframework.stereotype.Service

@Service
class LinkTransformationService {


    fun createLinkListFromNavigationStructure(tree: MyToysNavigation): List<Link> {
        val list = mutableListOf<Link>()
        tree.navigationEntries.forEach { section ->
            list.addAll(createLinkListFromMyToysNavigationEntries(section, null))
        }
        return list
    }

    fun createLinkListFromMyToysNavigationEntries(node: MyToysNavigationEntry, parentLabel: String?): List<Link> {
        val list = mutableListOf<Link>()
        val labelConcat = concatLabels(parentLabel, node)

        if (!node.children.isNullOrEmpty()) {
            node.children.forEach { child ->
                list.addAll(createLinkListFromMyToysNavigationEntries(child, labelConcat))
            }
        } else {
            list.add(Link(label = labelConcat, url = node.url!!))
        }
        return list
    }

    private fun concatLabels(parentLabel: String?, node: MyToysNavigationEntry): String {
        return if (parentLabel != null) {
            "$parentLabel - ${node.label}"
        } else node.label
    }
}