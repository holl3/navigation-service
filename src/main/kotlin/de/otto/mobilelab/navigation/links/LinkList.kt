package de.otto.mobilelab.navigation.links

data class Link(
    val label: String,
    val url: String
)