package de.otto.mobilelab.navigation.links

import org.springframework.stereotype.Service

@Service
class SortService {

    fun sortLinkList(linkList: List<Link>, sortOrder: Map<String, String>): List<Link> {

        val ordersWithValidKeys = sortOrder.filter { sortKeyComparators.containsKey(it.key)}

        if(ordersWithValidKeys.isEmpty()) return linkList

        val resultingOrder = ordersWithValidKeys
            .map { (fieldName, direction) ->
                sortKeyComparators[fieldName]!!.let { if (direction == "desc") it.reversed() else it }
            }
            .reduce { order, nextComparator -> order.then(nextComparator) }

        return linkList.sortedWith(resultingOrder)
    }

    companion object {
        private val sortKeyComparators = mapOf<String, Comparator<Link>>(
            "label" to compareBy { it.label },
            "url" to compareBy { it.url }
        )
    }
}
