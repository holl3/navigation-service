package de.otto.mobilelab.navigation.links

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class LinkController(
    private val linkService: LinkService
) {

    @GetMapping(value = ["/links"])
    fun getLinks(
        @RequestParam(name = "parent", required = false) parentName: String? = null,
        @RequestParam(name = "sort", required = false) sortString: String? = null
    ): List<Link> {

        return linkService.getLinkList(parentName, decomposeSort(sortString))
    }

    private fun decomposeSort(sortString: String?): Map<String, String> {
        val sortPairs = sortString?.split(",") ?: emptyList()

        if(isSortParameterEmpty(sortPairs)) return emptyMap()

        return sortPairs.map { sort ->
            val sortSplit = sort.split(":")
            val key = sortSplit[0]

            if (sortSplit.size > 1) {
                val direction = sortSplit[1]

                key to direction
            } else {
                key to DEFAULT_SORT_ORDER
            }

        }.toMap()
    }

    private fun isSortParameterEmpty(sortPairs: List<String>) = sortPairs.size == 1 && sortPairs[0].isEmpty()

    companion object{
        private const val DEFAULT_SORT_ORDER = "asc"
    }

}