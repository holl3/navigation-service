package de.otto.mobilelab.navigation

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import okhttp3.OkHttpClient
import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class NavigationServiceApplication {

    @Bean
    fun restClient(): OkHttpClient {
        return OkHttpClient()
    }

    @Bean
    fun objectMapper(): ObjectMapper {
        return jacksonObjectMapper()
    }

}

fun main(args: Array<String>) {
    runApplication<NavigationServiceApplication>(*args) {
        setBannerMode(Banner.Mode.OFF)
    }
}